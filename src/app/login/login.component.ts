import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { auth } from 'firebase';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  constructor(private _afAuth: AngularFireAuth, protected _router: Router) {}

  ngOnInit() {}

  login() {
    this._afAuth.auth.signInWithPopup(new auth.GoogleAuthProvider()).then(() => {
      this._router.navigate(['/main']);
    });
  }
}
