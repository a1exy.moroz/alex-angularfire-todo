import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login.component';
import { MatButtonModule } from '@angular/material/button';
import { LoginRoutingModule } from './login.routing.module';

@NgModule({
  imports: [CommonModule, MatButtonModule, LoginRoutingModule],
  exports: [LoginComponent],
  declarations: [LoginComponent],
})
export class LoginModule {}
