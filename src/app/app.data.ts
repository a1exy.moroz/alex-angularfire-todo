export interface RouteLink {
  title: string;
  route: Array<string>;
}
