import { Component, OnInit } from '@angular/core';
import { ChatService } from './chat.service';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { FormControl, Validators, FormGroup, FormGroupDirective } from '@angular/forms';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss'],
})
export class ChatComponent implements OnInit {
  chat$: Observable<any>;
  messageControl: FormControl;
  chatForm: FormGroup;

  constructor(private _chatService: ChatService, private _route: ActivatedRoute) {
    this.messageControl = new FormControl('', [Validators.required]);
    this.chatForm = new FormGroup({});
    this.chatForm.addControl('title', this.messageControl);
  }

  ngOnInit() {
    const chatId = this._route.snapshot.paramMap.get('id');
    const source = this._chatService.get(chatId);
    this.chat$ = this._chatService.joinUsers(source);
  }

  /**
   * Submit message
   * @chatId id of chat
   */
  submit(chatId, formDirective: FormGroupDirective) {
    this._chatService.sendMessage(chatId, this.messageControl.value);
    formDirective.resetForm();
  }

  /**
   * Filter messages by created date
   */
  trackByCreated(i, msg) {
    return msg.createdAt;
  }
}
