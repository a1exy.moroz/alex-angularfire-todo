import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { map, switchMap } from 'rxjs/operators';
import { firestore } from 'firebase';
import { MainService } from '../main.service';
import { Observable, of, combineLatest } from 'rxjs';

@Injectable()
export class ChatService {
  constructor(
    private afs: AngularFirestore,
    private router: Router,
    private _mainService: MainService,
  ) {}

  get(chatId) {
    return this.afs
      .collection<any>('chats')
      .doc(chatId)
      .snapshotChanges()
      .pipe(
        map((doc) => {
          const data = doc.payload.data() as object;
          return { id: doc.payload.id, ...data };
        }),
      );
  }

  joinUsers(chat$: Observable<any>) {
    let chat;
    const joinKeys = {};

    return chat$.pipe(
      switchMap((c) => {
        // Unique User IDs
        chat = c;
        const uids = Array.from(new Set(c.messages.map((v) => v.uid)));

        // Firestore User Doc Reads
        const userDocs = uids.map((u) => this.afs.doc(`users/${u}`).valueChanges());

        return userDocs.length ? combineLatest([userDocs[0]]) : of([]);
      }),
      map((arr) => {
        arr.forEach((v) => (joinKeys[v.uid] = v));
        chat.messages = chat.messages.map((v) => {
          return { ...v, user: joinKeys[v.uid] };
        });

        return chat;
      }),
    );
  }

  sendMessage(chatId, content) {
    const uid = this._mainService.user$.getValue().uid;
    const data = {
      uid,
      content,
      createdAt: Date.now(),
    };

    if (uid) {
      const ref = this.afs.collection('chats').doc(chatId);
      return ref.update({
        messages: firestore.FieldValue.arrayUnion(data),
      });
    }
  }
}
