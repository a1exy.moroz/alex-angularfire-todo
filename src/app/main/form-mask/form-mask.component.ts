import { BehaviorSubject } from 'rxjs';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { OrderRequest, END_POINT_ORDERS, START_BIRTHDAY } from './form-mask.data';
import { MatSnackBar } from '@angular/material';
import { AngularFirestore } from '@angular/fire/firestore';

@Component({
  selector: 'app-theatre',
  templateUrl: './form-mask.component.html',
  styleUrls: ['./form-mask.component.scss'],
})
export class FormMaskComponent implements OnInit {
  firstFormGroup: FormGroup;
  phoneControl: FormControl;

  secondFormGroup: FormGroup;
  nameControl: FormControl;
  surnameControl: FormControl;
  dateControl: FormControl;
  emailControl: FormControl;
  acceptRulesControl: FormControl;

  thirdFormGroup: FormGroup;
  cardNumberControl: FormControl;
  cardMonthYearControl: FormControl;
  ownerControl: FormControl;

  currentPerformance$: BehaviorSubject<Performance>;
  startDate = START_BIRTHDAY;

  constructor(
    private _formBuilder: FormBuilder,
    private _snackBar: MatSnackBar,
    private _angularFirestore: AngularFirestore,
  ) {
    this.currentPerformance$ = new BehaviorSubject(null);
  }

  ngOnInit() {
    this._initFirstFormGroup();
    this._initSecondFormGroup();
    this._initThirdFormGroup();
  }

  /**
   * Handler of select performance
   */
  onSelectPerformance(event): void {
    this.currentPerformance$.next(event.value);
  }

  /**
   * Handler of submit
   */
  onSubmit() {
    const data = {
      type: 'orders',
      attributes: {
        phone: this.phoneControl.value,
        first_name: this.nameControl.value,
        last_name: this.surnameControl.value,
        birthday: this.dateControl.value,
        email: this.emailControl.value,
        paypent: {
          card: {
            number: this.cardNumberControl.value,
            valid_thru: this.cardMonthYearControl.value,
            name: this.ownerControl.value,
          },
        },
      },
    };

    this.addOrder(data);
  }

  /**
   * Handler of bad submit
   */
  onBadSubmit() {
    this._snackBar.open(`The form isn't valid. Please, check the form.`, '', {
      duration: 5000,
    });
  }

  /**
   * Add order to the DB collection
   * @task data of order
   */
  addOrder(order: OrderRequest) {
    const collection = this._angularFirestore.collection<OrderRequest>(END_POINT_ORDERS);
    collection.add(order);
  }

  getNumberLength(value: number): number {
    return String(value).length;
  }

  /**
   * Initialization first form group
   */
  private _initFirstFormGroup(): void {
    this.firstFormGroup = this._formBuilder.group({});
    this.phoneControl = this._formBuilder.control('', [Validators.required]);
    this.firstFormGroup.addControl('performance', this.phoneControl);
  }

  /**
   * Initialization second form group
   */
  private _initSecondFormGroup(): void {
    this.secondFormGroup = this._formBuilder.group({});
    this.nameControl = this._formBuilder.control('', [Validators.required]);
    this.secondFormGroup.addControl('name', this.nameControl);
    this.surnameControl = this._formBuilder.control('', [Validators.required]);
    this.secondFormGroup.addControl('surname', this.surnameControl);
    this.dateControl = this._formBuilder.control('', []);
    this.secondFormGroup.addControl('date', this.dateControl);
    this.emailControl = this._formBuilder.control('', [Validators.required, Validators.email]);
    this.secondFormGroup.addControl('email', this.emailControl);
    this.acceptRulesControl = this._formBuilder.control(false, []);
    this.secondFormGroup.addControl('accept-rules', this.acceptRulesControl);
  }

  /**
   * Initialization third form group
   */
  private _initThirdFormGroup(): void {
    this.thirdFormGroup = this._formBuilder.group({});
    this.cardNumberControl = this._formBuilder.control('', [Validators.required]);
    this.thirdFormGroup.addControl('card-number', this.cardNumberControl);
    this.cardMonthYearControl = this._formBuilder.control('', [Validators.required]);
    this.thirdFormGroup.addControl('card-month', this.cardMonthYearControl);
    this.ownerControl = this._formBuilder.control('', [Validators.required]);
    this.thirdFormGroup.addControl('email', this.ownerControl);
  }
}
