export const END_POINT_ORDERS = 'orders';
export const START_BIRTHDAY = new Date(1990, 0, 1);

export interface OrderRequest {
  type: string;
  attributes: OrderAttributesRequest;
}

interface OrderAttributesRequest {
  phone: number;
  first_name: string;
  last_name: string;
  birthday: string;
  email: string;
  paypent: OrderPaypentRequest;
}

interface OrderPaypentRequest {
  card: OrderCardRequest;
}

interface OrderCardRequest {
  number: number;
  valid_thru: string;
  name: string;
}
