import { Component, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import { RouteLink } from '../app.data';
import { MainService } from './main.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss'],
})
export class MainComponent implements OnInit {
  changeThemeControl: FormControl;
  activeLink: string;
  links: RouteLink[];

  constructor(
    protected _router: Router,
    public afAuth: AngularFireAuth,
    public mainService: MainService,
  ) {}

  ngOnInit(): void {
    const splitUrl = this._router.url.split('/');
    this.activeLink = splitUrl[2];
    this.links = [
      {
        route: ['tasks'],
        title: 'Tasks',
      },
      {
        route: ['form-mask'],
        title: 'Form mask',
      },
      {
        route: ['chats', 'xeBj5TRdFFmWjcIJ6aSU'],
        title: 'Chat',
      },
    ];
    this._setDarkTheme();
  }

  /**
   * Handler on logout
   */
  logout() {
    this.afAuth.auth.signOut().then(() => {
      this._router.navigate(['/login']);
    });
  }

  /**
   * Set dark theme
   */
  private _setDarkTheme(): void {
    const darkTheme = localStorage.getItem('dark-theme') === 'true';
    this.changeThemeControl = new FormControl(darkTheme, []);
    this.changeThemeControl.valueChanges.subscribe((isChange) => {
      localStorage.setItem('dark-theme', isChange);
      document.querySelector('.app__theme_default').classList.toggle('app__theme_dark-theme');
    });
  }
}
