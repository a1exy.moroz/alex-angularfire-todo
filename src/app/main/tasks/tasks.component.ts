import { Component, OnInit } from '@angular/core';
import { RouteLink } from 'src/app/app.data';
import { Router } from '@angular/router';
import { TaskService } from './tasks.service';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent implements OnInit {
  activeLink: string;
  links: RouteLink[];
  constructor(private _taskService: TaskService, protected _router: Router) {
    const splitUrl = this._router.url.split('/');
    this.activeLink = splitUrl[splitUrl.length - 1];
    this.links = [
      {
        route: ['table'],
        title: 'Table',
      },
      {
        route: ['scrum'],
        title: 'Scrum',
      },
    ];
  }

  ngOnInit() {}

  /**
   * Handler on create task
   */
  onCreateTask(): void {
    this._taskService.createTaskDialog();
  }
}
