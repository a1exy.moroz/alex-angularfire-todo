import { TaskService } from './../tasks.service';
import { FormControl } from '@angular/forms';
import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { BehaviorSubject, fromEvent, merge } from 'rxjs';
import { Router } from '@angular/router';
import { TasksDataSource } from './table.datasource';
import { debounceTime, distinctUntilChanged, tap } from 'rxjs/operators';
import { Task } from '../tasks.data';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild('inputFilter') inputFilter: ElementRef;
  @ViewChild(MatSort) sort: MatSort;

  displayedColumns: Array<string> = ['title', 'start', 'end', 'type', 'actions'];
  titleFilterControl: FormControl;
  dataSource: TasksDataSource;
  loading$: BehaviorSubject<boolean>;

  constructor(private _taskService: TaskService, protected _router: Router) {
    this.loading$ = new BehaviorSubject(true);
  }

  ngOnInit() {
    this._taskService.initTasks();
    this.dataSource = new TasksDataSource();
    this._taskService.tasks$.subscribe((response: Task[]) => {
      this.loading$.next(false);
      this.dataSource.loadTasks(response);
      this._updateTasksPage();
    });
  }

  ngAfterViewInit() {
    // server-side search
    fromEvent(this.inputFilter.nativeElement, 'keyup')
      .pipe(
        debounceTime(150),
        distinctUntilChanged(),
        tap(() => {
          this.paginator.pageIndex = 0;
          this._updateTasksPage();
        }),
      )
      .subscribe();

    // reset the paginator after sorting
    this.sort.sortChange.subscribe(() => (this.paginator.pageIndex = 0));

    // on sort or paginate events, update a new page
    merge(this.sort.sortChange, this.paginator.page)
      .pipe(tap(() => this._updateTasksPage()))
      .subscribe();
  }

  ngOnDestroy() {
    this.loading$.complete();
  }

  /**
   * Handler of update task dialog
   * @task data of task
   */
  onEditTask(task: Task) {
    this._taskService.editTaskDialog(task);
  }

  /**
   * Handler of information task dialog
   * @task data of task
   */
  onInfoTask(task: Task) {
    this._router.navigate(['/main/info', task.id]);
  }

  /**
   * Handler of remove task dialog
   * @task data of task
   */
  onRemoveTask(task: Task) {
    this._taskService.deleteTask(task.id);
  }

  /**
   * Update page tasks
   */
  private _updateTasksPage() {
    this.dataSource.updateTasks(
      this.inputFilter.nativeElement.value,
      'title',
      this.sort.active,
      this.sort.direction,
      this.paginator.pageIndex,
      this.paginator.pageSize,
    );
  }
}
