import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Injectable } from '@angular/core';
import { Task } from '../tasks.data';

@Injectable()
export class TasksDataSource implements DataSource<Task> {
  private tasksView$ = new BehaviorSubject<Task[]>([]);
  private _tasksAll: Task[];

  public tasks$ = new BehaviorSubject<Task[]>([]);

  constructor() {
    this._tasksAll = [];
  }

  connect(): Observable<Task[]> {
    return this.tasksView$.asObservable();
  }

  disconnect(): void {
    this.tasks$.complete();
    this.tasksView$.complete();
  }

  loadTasks(tasks: Task[]) {
    this.tasks$.next(tasks);
    this.tasksView$.next(tasks);
    this._tasksAll = tasks;
  }

  /**
   * The tasks was updated
   * @param filter filter value
   * @param filterActive filter column active
   * @param sortActive active column
   * @param sortDirection sort direction
   * @param pageIndex number of page
   * @param pageSize page size
   */
  updateTasks(
    filter: string,
    filterActive: string,
    sortActive: string,
    sortDirection: string,
    pageIndex: number,
    pageSize: number,
  ) {
    let tasks = this._filterTasks(filterActive, filter);
    tasks = this._sortTasks(tasks, sortActive, sortDirection);
    tasks = this._pageTasks(tasks, pageIndex, pageSize);
    this.tasksView$.next(tasks);
  }

  /**
   * The tasks was filtered by column
   * @param filterActive active column
   * @param filter filter value
   */
  private _filterTasks(filterActive: string, filter: string): Task[] {
    const filterTasks = this._tasksAll.filter((element) => element[filterActive].includes(filter));
    this.tasks$.next(filterTasks);
    return filterTasks;
  }

  /**
   * The tasks was sorted by column
   * @param tasks tasks
   * @param sortActive active column
   * @param sortDirection sort direction
   */
  private _sortTasks(tasks: Task[], sortActive: string, sortDirection: string): Task[] {
    return tasks.sort((a, b) => {
      const isAsc = sortDirection === 'asc';
      switch (sortActive) {
        case 'title':
          return this._compareTwoValues(a.title, b.title, isAsc);
        case 'start':
          return this._compareTwoValues(a.start, b.start, isAsc);
        case 'end':
          return this._compareTwoValues(a.start, b.start, isAsc);
        case 'type':
          return this._compareTwoValues(a.type.name, b.type.name, isAsc);
        default:
          return 0;
      }
    });
  }

  /**
   * Compare two values for sorting table
   * @param first first value
   * @param second second value
   * @param isAsc sort direction, if TRUE from smaller to bigger, if FALSE from bigger to smaller
   */
  private _compareTwoValues(first: number | string, second: number | string, isAsc: boolean) {
    return (first < second ? -1 : 1) * (isAsc ? 1 : -1);
  }

  /**
   * The tasks was filter by page
   * @param tasks tasks
   * @param pageIndex number of page
   * @param pageSize page size
   */
  private _pageTasks(tasks: Task[], pageIndex: number, pageSize: number): Task[] {
    const start = pageIndex * pageSize;
    const end = start + pageSize;
    return tasks.filter((element, index) => {
      return index >= start && index < end;
    });
  }
}
