import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableComponent } from './table.component';
import { TableRoutingModule } from './table.routing.module';
import {
  MatTableModule,
  MatSortModule,
  MatIconModule,
  MatMenuModule,
  MatButtonModule,
  MatInputModule,
  MatProgressSpinnerModule,
  MatPaginatorModule,
  MatFormFieldModule,
} from '@angular/material';
import { TasksDataSource } from './table.datasource';

@NgModule({
  imports: [
    CommonModule,
    TableRoutingModule,
    MatTableModule,
    MatIconModule,
    MatMenuModule,
    MatButtonModule,
    MatInputModule,
    MatProgressSpinnerModule,
    MatPaginatorModule,
    MatSortModule,
    MatFormFieldModule,
  ],
  declarations: [TableComponent],
  exports: [TableComponent],
  providers: [TasksDataSource],
})
export class TableModule {}
