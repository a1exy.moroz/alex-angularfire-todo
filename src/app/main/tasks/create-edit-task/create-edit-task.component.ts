import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { TaskType, Task, TYPE_OPTIONS } from '../tasks.data';

@Component({
  selector: 'app-create-edit-task',
  templateUrl: './create-edit-task.component.html',
  styleUrls: ['./create-edit-task.component.scss'],
})
export class CreateEditTaskComponent implements OnInit {
  typeOptions: TaskType[];

  taskForm: FormGroup;
  titleControl: FormControl;
  descriptionControl: FormControl;
  startDateControl: FormControl;
  endDateControl: FormControl;
  typeControl: FormControl;
  isEdit: boolean;
  title: string;
  minDate: Date;

  constructor(
    public dialogRef: MatDialogRef<CreateEditTaskComponent>,
    private _formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: Task,
  ) {
    this.typeOptions = TYPE_OPTIONS;
    this.isEdit = data.title.length !== 0;
    this.title = this.isEdit ? 'Edit task' : 'Create task';
    this._createForm(this.data);
  }

  ngOnInit() {
    this.minDate = new Date();
  }

  /**
   * Handler on save dialog
   */
  onSubmit() {
    const data = {
      title: this.titleControl.value,
      description: this.descriptionControl.value,
      type: this.typeControl.value,
      create: this.data.create,
      start: new Date(this.startDateControl.value).getTime(),
      end: new Date(this.endDateControl.value).getTime(),
      owner: this.data.owner,
    };
    this.dialogRef.close(data);
  }

  /**
   * Handler on close dialog
   */
  onClose() {
    this.dialogRef.close();
  }

  /**
   * Handler on close dialog
   */
  isDisabledSave() {
    return !this.taskForm.valid || !this.taskForm.dirty;
  }

  /**
   * Create form when we are editing task
   * @task data of task
   */
  private _createForm(task: Task) {
    this.taskForm = this._formBuilder.group({});
    // Title
    this.titleControl = this._formBuilder.control(task.title, [Validators.required]);
    this.taskForm.addControl('title', this.titleControl);
    // Description
    this.descriptionControl = this._formBuilder.control(task.description, [Validators.required]);
    this.taskForm.addControl('description', this.descriptionControl);
    // Date of start
    this.startDateControl = this._formBuilder.control(new Date(task.start), [Validators.required]);
    this.taskForm.addControl('start-date', this.startDateControl);
    // Date of end
    this.endDateControl = this._formBuilder.control(new Date(task.end), [Validators.required]);
    this.taskForm.addControl('start-end', this.endDateControl);
    // Type
    task.type = this.typeOptions.find((element) => task.type.key === element.key);
    this.typeControl = this._formBuilder.control(task.type, []);
    this.taskForm.addControl('type', this.typeControl);
  }
}
