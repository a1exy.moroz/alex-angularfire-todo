import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateEditTaskComponent } from './create-edit-task.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material';
import { DirectiveModule } from 'src/assets/directive/directive.module';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    DirectiveModule,
  ],
  declarations: [CreateEditTaskComponent],
  exports: [CreateEditTaskComponent],
})
export class CreateEditTaskModule {}
