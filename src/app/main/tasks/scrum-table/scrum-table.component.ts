import { Component, OnInit } from '@angular/core';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { TaskService } from '../tasks.service';
import { TaskType, TYPE_OPTIONS, TaskTypes, Task } from '../tasks.data';

@Component({
  selector: 'app-scrum-table',
  templateUrl: './scrum-table.component.html',
  styleUrls: ['./scrum-table.component.scss'],
})
export class ScrumTableComponent implements OnInit {
  types: TaskType[];

  constructor(public taskService: TaskService) {
    this.types = TYPE_OPTIONS;
  }

  ngOnInit() {
    this.taskService.initTasks();
  }

  /**
   * Handler on drop
   * @event drop even
   */
  onDrop(event: CdkDragDrop<any>) {
    if (event.previousContainer === event.container) {
      moveItemInArray(event.container.data, event.previousIndex, event.currentIndex);
    } else {
      const key = event.container.id as TaskTypes;
      this._updateDropTask(event.item.data, key);
      transferArrayItem(
        event.previousContainer.data,
        event.container.data,
        event.previousIndex,
        event.currentIndex,
      );
    }
  }

  /**
   * Update task data for drop event
   * @task task date
   */
  private _updateDropTask(task: Task, key: TaskTypes): void {
    const taskId = task.id;
    const updateTask = { ...task };
    delete updateTask.id;
    updateTask.type = this.types.find((element) => element.key === key);
    this.taskService.updateTask(taskId, updateTask);
  }
}
