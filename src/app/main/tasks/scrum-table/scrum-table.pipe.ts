import { PipeTransform, Pipe } from '@angular/core';
import { Task } from '../tasks.data';

/**
 * Pipe for tasks filter by key
 */
@Pipe({
  name: 'filterByKey',
})
export class FilterByKeyPipe implements PipeTransform {
  transform(tasks: Task[], key: string): Task[] {
    return tasks.filter((task) => task.type.key === key);
  }
}
