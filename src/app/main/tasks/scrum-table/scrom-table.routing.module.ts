import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ScrumTableComponent } from './scrum-table.component';

const routes: Routes = [
  {
    path: '',
    component: ScrumTableComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ScrumTableRoutingModule {}
