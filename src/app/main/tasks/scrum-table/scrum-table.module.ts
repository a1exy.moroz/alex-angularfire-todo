import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrumTableComponent } from './scrum-table.component';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { TaskModule } from './task/task.module';
import { ScrumTableRoutingModule } from './scrom-table.routing.module';
import { FilterByKeyPipe } from './scrum-table.pipe';

@NgModule({
  imports: [CommonModule, DragDropModule, TaskModule, ScrumTableRoutingModule],
  declarations: [ScrumTableComponent, FilterByKeyPipe],
  exports: [ScrumTableComponent],
})
export class ScrumTableModule {}
