import { Component, OnInit, Input } from '@angular/core';
import { Task, TaskTypes } from '../../tasks.data';
import { TaskService } from '../../tasks.service';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent implements OnInit {
  @Input() task: Task;
  constructor(private _taskService: TaskService) {}

  ngOnInit() {}
  /**
   * Handler on update task
   */
  onEditTask(): void {
    this._taskService.editTaskDialog(this.task);
  }

  /**
   * Handler on delete task
   */
  onDeleteTask(): void {
    this._taskService.deleteTask(this.task.id);
  }

  getTaskStyle(type: TaskTypes): string {
    let classType = 'task__';
    switch (type) {
      case TaskTypes.PLANNING:
        classType += 'planning';
        break;
      case TaskTypes.IN_PROGRESS:
        classType += 'in-progress';
        break;
      case TaskTypes.DONE:
        classType += 'done';
        break;
    }
    return classType;
  }
}
