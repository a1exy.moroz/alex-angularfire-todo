import { MainService } from './../main.service';
import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { CreateEditTaskComponent } from './create-edit-task/create-edit-task.component';
import { takeWhile, map } from 'rxjs/operators';
import { AngularFirestore } from '@angular/fire/firestore';
import { END_POINT, Task, TYPE_OPTIONS } from './tasks.data';
import { Observable } from 'rxjs';

@Injectable()
export class TaskService {
  dialogWidth: number;
  tasks$: Observable<Task[]>;

  constructor(
    private _dialog: MatDialog,
    private _angularFirestore: AngularFirestore,
    private _mainService: MainService,
  ) {
    this.dialogWidth = 300;
  }

  /**
   * Initial observable tasks
   */
  initTasks() {
    this.tasks$ = this._observableTasks(END_POINT);
  }

  /**
   * Create task dialog
   */
  createTaskDialog() {
    const createData = {
      title: '',
      description: '',
      type: TYPE_OPTIONS[0],
      create: new Date().getTime(),
      start: new Date().getTime(),
      end: new Date().getTime(),
      owner: this._mainService.user$.getValue().displayName,
    };

    this._openDialog(createData).subscribe((response: Task) => {
      this.addTask(response);
    });
  }

  /**
   * Update task dialog
   * @task data of task
   */
  editTaskDialog(task: Task) {
    this._openDialog(task).subscribe((response: Task) => {
      this.updateTask(task.id, response);
    });
  }

  /**
   * Add task to the DB collection
   * @task data of task
   */
  addTask(task: Task) {
    const collection = this._angularFirestore.collection<Task>(END_POINT);
    collection.add(task);
  }

  /**
   * Update task from the DB collection
   * @id id of task
   * @task data of task
   */
  updateTask(id: string, task: Task) {
    const taskDoc = this._angularFirestore.doc<Task>(`${END_POINT}/${id}`);
    taskDoc.set(task);
  }

  /**
   * Delete task from the DB collection
   * @id id of task
   */
  deleteTask(id: string) {
    const taskDoc = this._angularFirestore.doc<Task>(`${END_POINT}/${id}`);
    taskDoc.delete();
  }

  /**
   * Open mat dialog
   * @task data of task
   */
  private _openDialog(task): Observable<Task> {
    const dialogRef = this._dialog.open(CreateEditTaskComponent, {
      width: `${this.dialogWidth}px`,
      data: task,
    });

    return dialogRef.afterClosed().pipe(takeWhile((response) => response !== undefined));
  }

  /**
   * Return observable tasks
   * @point end point for request
   */
  private _observableTasks(point: string): Observable<Task[]> {
    return this._angularFirestore
      .collection(point)
      .snapshotChanges()
      .pipe(
        map((actions) => {
          const tasks = actions.map((a) => {
            // Get document data
            const data = a.payload.doc.data() as Task;
            // Get document id
            const id = a.payload.doc.id;
            // Use spread operator to add the id to the document data

            return { id, ...data };
          });
          return tasks;
        }),
      );
  }
}
