import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TasksComponent } from './tasks.component';
import { MatButtonModule, MatTabsModule, MatIconModule, MatDialogModule } from '@angular/material';
import { TasksRoutingModule } from './tasks.routing.module';
import { CreateEditTaskModule } from './create-edit-task/create-edit-task.module';
import { CreateEditTaskComponent } from './create-edit-task/create-edit-task.component';
import { TaskService } from './tasks.service';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MatTabsModule,
    MatIconModule,
    TasksRoutingModule,
    CreateEditTaskModule,
    MatDialogModule,
  ],
  exports: [TasksComponent],
  declarations: [TasksComponent],
  bootstrap: [CreateEditTaskComponent],
  providers: [TaskService],
})
export class TasksModule {}
