export const END_POINT = 'tasks';

export interface RouteLink {
  title: string;
  route: string;
}

export interface Task {
  id?: string | null;
  description: string;
  create: number;
  title: string;
  type: TaskType;
  start: number;
  end: number;
  owner: string;
}

export interface TaskType {
  key: TaskTypes;
  name: string;
}
// Types of tasks
export enum TaskTypes {
  PLANNING = 'PLANNING',
  IN_PROGRESS = 'IN_PROGRESS',
  DONE = 'DONE',
}

export const TYPE_OPTIONS: TaskType[] = [
  {
    key: TaskTypes.PLANNING,
    name: 'Planning',
  },
  {
    key: TaskTypes.IN_PROGRESS,
    name: 'In progress',
  },
  {
    key: TaskTypes.DONE,
    name: 'Done',
  },
];

export interface FilterValues {
  title: string;
}
