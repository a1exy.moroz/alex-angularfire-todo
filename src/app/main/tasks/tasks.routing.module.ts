import { Routes, RouterModule } from '@angular/router';

import { TasksComponent } from './tasks.component';

import { NgModule } from '@angular/core';

export const routes: Routes = [
  {
    path: '',
    component: TasksComponent,
    children: [
      {
        path: 'table',
        loadChildren: () => import('./table/table.module').then((m) => m.TableModule),
      },
      {
        path: 'scrum',
        loadChildren: () =>
          import('./scrum-table/scrum-table.module').then((m) => m.ScrumTableModule),
      },
      {
        path: '',
        redirectTo: 'table',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TasksRoutingModule {}
