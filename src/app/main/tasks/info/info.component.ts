import { TaskService } from '../tasks.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Task, END_POINT } from '../tasks.data';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss'],
})
export class InfoComponent implements OnInit {
  task$: Observable<Task>;

  constructor(private _angularFirestore: AngularFirestore, protected _router: Router) {
    const splitUrl = this._router.url.split('/');
    const id = splitUrl[splitUrl.length - 1];
    this.task$ = this._angularFirestore.doc<Task>(`${END_POINT}/${id}`).valueChanges();
  }

  ngOnInit() {}

  onBack() {
    this._router.navigate(['/main/tasks/table']);
  }
}
