import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoComponent } from './info.component';
import { InfoRoutingModule } from './info.routing.module';
import { MatButtonModule, MatIconModule, MatCardModule } from '@angular/material';
import { TaskService } from '../tasks.service';

@NgModule({
  imports: [CommonModule, InfoRoutingModule, MatButtonModule, MatIconModule, MatCardModule],
  declarations: [InfoComponent],
  exports: [InfoComponent],
})
export class InfoModule {}
