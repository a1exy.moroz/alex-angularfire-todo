import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { User } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';

@Injectable()
export class MainService {
  user$: BehaviorSubject<User>;

  constructor(private _afAuth: AngularFireAuth, private afs: AngularFirestore) {
    this._afAuth.user.subscribe((response: User) => {
      const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${response.uid}`);

      const data = {
        uid: response.uid,
        email: response.email,
        displayName: response.displayName,
        photoURL: response.photoURL,
      };

      userRef.set(data, { merge: true });
      this.user$ = new BehaviorSubject(response);
    });
  }
}
