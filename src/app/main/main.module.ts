import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainComponent } from './main.component';
import { MainRoutingModule } from './main.routing.module';
import { MatButtonModule, MatTabsModule, MatSlideToggleModule } from '@angular/material';
import { MainService } from './main.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    MatButtonModule,
    MainRoutingModule,
    MatTabsModule,
    MatSlideToggleModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  exports: [MainComponent],
  declarations: [MainComponent],
  providers: [MainService],
})
export class MainModule {}
