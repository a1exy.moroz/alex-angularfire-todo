import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponent } from './main.component';

export const routes: Routes = [
  {
    path: '',
    component: MainComponent,
    children: [
      {
        path: 'tasks',
        loadChildren: () => import('./tasks/tasks.module').then((m) => m.TasksModule),
      },
      {
        path: 'info/:id',
        loadChildren: () => import('./tasks/info/info.module').then((m) => m.InfoModule),
      },
      {
        path: 'form-mask',
        loadChildren: () => import('./form-mask/form-mask.module').then((m) => m.FormMaskModule),
      },
      {
        path: 'chats/:id',
        loadChildren: () => import('./chat/chat.module').then((m) => m.ChatModule),
      },
      {
        path: '',
        redirectTo: 'tasks',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MainRoutingModule {}
