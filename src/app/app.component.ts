import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {
    if (localStorage.getItem('dark-theme') === 'true') {
      document.querySelector('.app__theme_default').classList.toggle('app__theme_dark-theme');
    }
  }
}
