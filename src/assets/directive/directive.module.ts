import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ScrollIntoViewDirective } from './scroll-into-view.directive';
import { AutofocusDirective } from './create-edit-task.directive';

@NgModule({
  imports: [CommonModule],
  declarations: [ScrollIntoViewDirective, AutofocusDirective],
  exports: [ScrollIntoViewDirective, AutofocusDirective],
})
export class DirectiveModule {}
