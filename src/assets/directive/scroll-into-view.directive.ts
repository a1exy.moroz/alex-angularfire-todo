import { Directive, ElementRef, Input, AfterViewInit } from '@angular/core';

@Directive({
  selector: '[scrollIntoView]',
})
export class ScrollIntoViewDirective implements AfterViewInit {
  @Input() scrollIntoView: boolean;

  constructor(private _elementRef: ElementRef) {}

  ngAfterViewInit() {
    if (this.scrollIntoView) {
      this._elementRef.nativeElement.scrollIntoView();
    }
  }
}
