import { Directive, AfterViewInit, ElementRef, ChangeDetectorRef, Input } from '@angular/core';

@Directive({
  selector: '[autofocus]',
})
export class AutofocusDirective implements AfterViewInit {
  private _autofocus;
  constructor(private el: ElementRef, private _cdr: ChangeDetectorRef) {}

  ngAfterViewInit() {
    if (this._autofocus || typeof this._autofocus === 'undefined') {
      setTimeout(() => {
        this.el.nativeElement.focus();
        this._cdr.markForCheck();
      }, 500);
    }
  }

  @Input() set autofocus(condition: boolean) {
    this._autofocus = condition;
  }
}
